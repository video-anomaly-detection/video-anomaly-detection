import os.path
import warnings
import numpy as np
import skvideo.io
import ffmpeg

import prednet.prednet
import prednet.data_input
import prednet.data_utils

import keras.layers
import keras.callbacks
import keras.models
import keras.backend


def train_on_hickles(DATA_DIR,
                     number_of_epochs=150, steps_per_epoch=125,
                     path_to_save_model_json='prednet_model.json', path_to_save_weights_hdf5='prednet_weights.hdf5',
                     ):
  # Data files
  train_file = os.path.join(DATA_DIR, 'X_train.hkl')
  train_sources = os.path.join(DATA_DIR, 'sources_train.hkl')
  val_file = os.path.join(DATA_DIR, 'X_validate.hkl')
  val_sources = os.path.join(DATA_DIR, 'sources_validate.hkl')
  return train_on_arrays_and_sources(train_file, train_sources, val_file, val_sources,
                                     path_to_save_model_json=path_to_save_model_json, path_to_save_weights_hdf5=path_to_save_weights_hdf5,
                                     number_of_epochs=number_of_epochs, steps_per_epoch=steps_per_epoch)


def default_path_to_save_model(path_to_video):
  return os.path.splitext(path_to_video)[0] + '.model.save.hdf5'


def make_reduced_video(path_to_video, frame_shape):
  """
  https://trac.ffmpeg.org/wiki/Scaling
   Sometimes you want to scale an image, but avoid upscaling it if its dimensions are too low.
   This can be done using min expressions:
  ffmpeg -i input.jpg -vf "scale='min(320,iw)':'min(240,ih)'" input_not_upscaled.png
  But do we want to? If we have too many total pixels, more than we can fit on the GPU, it doesn't matter if one direction is okay.
  http://ffmpeg.org/ffmpeg-filters.html#scale

  interactive widget in jupyter https://github.com/kkroening/ffmpeg-python/blob/master/examples/ffmpeg-numpy.ipynb
  https://raw.githubusercontent.com/kkroening/ffmpeg-python/master/doc/jupyter-demo.gif
  """
  noExtension, extension = os.path.splitext(path_to_video)
  path_to_scaled_video = noExtension + '_' + str(frame_shape[0]) + '_' + str(frame_shape[1]) + extension
  ffmpeg.input(path_to_video).filter('scale', frame_shape[0], frame_shape[1]).output(path_to_scaled_video).overwrite_output().run()
  return path_to_scaled_video


def reduce_frame_rate(video_array, fraction_of_frames_to_keep=1.0):
  numberOfFramesToKeep = int(fraction_of_frames_to_keep * video_array.shape[0])
  frameStep = video_array.shape[0]/numberOfFramesToKeep
  if np.rint(frameStep) != frameStep:
    #warnings.warn("The neural network works by learning what changes to expect from one frame to the next."
    #              "If the video is not at a consistent speed, this is more difficult to do."
    #              "Not impossible, so if you must reduce a video to 3/4 or such, it can be done."
    #              "But predictive performance may suffer.")
    frameStep = np.rint(frameStep)
    warnings.warn("When reducing the framerate, normalized the stride length to the integer {}".format(frameStep))
  # There are multiple different, mathematically-incompatible definitions of a good fit here.
  # What we are doing is prioritizing each individual frame being as close as possible to where it should be.
  # This necessarily means that the frames are not necessarily evenly spaced, so the video "speeds up" or "slows down" a small amount.
  # It will always speed up a little and immediately slow down a little, such that after e.g. 30 seconds we're on the frame nearest the 30-second mark.
  # framesToKeep = np.rint(np.linspace(0, video_array.shape[0]-1, numberOfFramesToKeep)).astype(np.int)
  # framesToKeep = np.rint(np.arange(0, video_array.shape[0], frameStep)).astype(np.int)
  framesToKeep = np.arange(0, video_array.shape[0], frameStep).astype(np.int)
  assert framesToKeep[0] == 0
  #assert framesToKeep[-1] == video_array.shape[0] - 1
  #assert framesToKeep.shape == (numberOfFramesToKeep,)
  assert np.all(framesToKeep[1:] - framesToKeep[:-1] == frameStep)
  video_array = video_array[framesToKeep]
  #assert video_array.shape[0] == numberOfFramesToKeep
  return video_array


def train_on_single_video(path_to_video,
                          path_to_save_model_json=None, path_to_save_weights_hdf5=None,
                          path_to_save_model_file=None,
                          number_of_epochs=150, steps_per_epoch=125,
                          batch_size=4,
                          sequence_length=8,
                          fraction_to_use_for_validation=0,
                          fraction_of_frames_to_keep=1.0,
                          max_pixels_per_frame=None,
                          retrain=False,
                          ):
  """
  Picking which frames to use for validation is tricky, because when using sequence_start_mode='all',
  we require the video to have no jumps in it; we need to be able to start a sequence anywhere.
  Thus, the only place we can really scythe out a section for validation is at the beginning or the end.
  """
  if not path_to_save_model_json:
    path_to_save_model_json = os.path.splitext(path_to_video)[0] + '.model.json'
  if not path_to_save_weights_hdf5:
    path_to_save_weights_hdf5 = os.path.splitext(path_to_video)[0] + '.model.hdf5'
  if not path_to_save_model_file:
    path_to_save_model_file = default_path_to_save_model(path_to_video)
  path_to_save_settings = os.path.splitext(path_to_save_model_file)[0] + '.settings.txt'
  print('number_of_epochs =', number_of_epochs, 'steps_per_epoch =', steps_per_epoch,
        file=open(path_to_save_settings, 'w'))
  # Better to check in the evaluate function whether training is already done.
  # If the train function gets a command to re-train (possibly on a new video), then re-train.
  if path_to_save_model_file and os.path.exists(path_to_save_model_file) and not retrain:
    # For this special case, do not re-train if we already have a trained model.
    print('train_on_single_video found', path_to_save_model_file,
          'so just using that instead of re-training.')
    # Should we enable training on multiple videos sequentially?
    return
  if os.path.exists(path_to_save_model_json) and os.path.exists(path_to_save_weights_hdf5):
    # For this special case, do not re-train if we already have a trained model.
    print('train_on_single_video found', path_to_save_model_json, 'and', path_to_save_weights_hdf5,
          'so just using those instead of re-training.')
    if path_to_save_model_file and not os.path.exists(path_to_save_model_file):
      with open(path_to_save_model_json) as f:
        json_string = f.read()
      model = keras.models.model_from_json(json_string, custom_objects = {'PredNet': prednet.prednet.PredNet})
      model.load_weights(path_to_save_weights_hdf5)
      # When we upgrade to TensorFlow 2, this will need to specify HDF5 format: model.save(path_to_save_model_file, save_format='h5')
      model.save(path_to_save_model_file)
    return

  try:
    json = ffmpeg.probe(path_to_video)
  except Exception as ex:
    raise ValueError(path_to_video + str(ex))
  # strangely, 'width' and 'height' are not in the JSON on Travis
  if 'width' in json and 'height' in json:
    totalPixels = json['width'] * json['height']
    if max_pixels_per_frame and max_pixels_per_frame < totalPixels:
      reductionFactor = max_pixels_per_frame/totalPixels
      newWidth = int(json['width'] * reductionFactor)
      newHeight = int(json['height'] * reductionFactor)
      # (width, height)
      # make_reduced_video(path_to_video, (newWidth, newHeight))
  else:
    pass
    # print(json, '= ffmpeg.probe({})'.format(path_to_video))

  # print('train_on_single_video about to call skvideo.io.vread')
  array = skvideo.io.vread(path_to_video)

  array = reduce_frame_rate(array, fraction_of_frames_to_keep)

  # print('train_on_single_video returned from skvideo.io.vread')
  source_list = [path_to_video for frame in array]
  assert len(source_list) == array.shape[0]
  numberOfFrames = array.shape[0]
  assert len(source_list) == numberOfFrames
  number_of_validation_sequences = int(numberOfFrames * fraction_to_use_for_validation / sequence_length)
  numberOfValidationFrames = number_of_validation_sequences * sequence_length
  # numberOfValidationFrames might be zero. array[:-0] is equivalent to array[:0] which is empty.
  numberOfTrainingFrames = array.shape[0] - numberOfValidationFrames
  assert numberOfTrainingFrames > 0
  return train_on_arrays_and_sources(array[:numberOfTrainingFrames], source_list[:numberOfTrainingFrames],
                                     array[numberOfTrainingFrames:], source_list[numberOfTrainingFrames:],
                                     path_to_save_model_json=path_to_save_model_json,
                                     path_to_save_weights_hdf5=path_to_save_weights_hdf5,
                                     model_path=path_to_save_model_file,
                                     number_of_epochs=number_of_epochs, steps_per_epoch=steps_per_epoch,
                                     batch_size=batch_size, sequence_length=sequence_length,
                                     )



def train_on_single_path(path,
                         path_to_save_model_file=None,
                         number_of_epochs=150, steps_per_epoch=125,
                         *args, **kwargs):
  foundAvideo = False
  videos = list()
  for filepath in prednet.data_input.walk_videos(path):
    print('PredNet training on', filepath)
    foundAvideo = True
    videos.append(skvideo.io.vread(filepath))
    # train_on_single_video(path, path_to_save_model_file=path_to_save_model_file,
    #                      number_of_epochs=number_of_epochs, steps_per_epoch=steps_per_epoch,
    #                      *args, **kwargs)
  if not foundAvideo:
    raise ValueError('No videos found in ' + path)
  concatenatedVideoPath = os.path.splitext(path_to_save_model_file)[0] + '.mp4'
  skvideo.io.vwrite(concatenatedVideoPath, np.concatenate(videos))
  train_on_single_video(concatenatedVideoPath, path_to_save_model_file=path_to_save_model_file,
                        number_of_epochs=number_of_epochs, steps_per_epoch=steps_per_epoch,
                        *args, **kwargs)
  assert os.path.exists(path_to_save_model_file)


def train_on_video_list(paths_to_videos,
                        path_to_save_model_file,
                        number_of_epochs=150, steps_per_epoch=125,
                        *args, **kwargs):
  """
  train_on_video_list is literally just a convenience function that calls train_on_single_video in a loop.
  """
  for path_to_video in paths_to_videos:
    train_on_single_path(path_to_video, path_to_save_model_file=path_to_save_model_file,
                         number_of_epochs=number_of_epochs, steps_per_epoch=steps_per_epoch,
                         *args, **kwargs)


def make_training_model(nt, frame_shape):
  """
  The model depends on the input shape: the height, width, and number of channels.
  A model trained on one frame shape will not be applicable to another frame shape.
  """
  # Model parameters
  n_channels = 3
  # frame_shape = (n_channels, im_height, im_width) if keras.backend.image_data_format() == 'channels_first' else (im_height, im_width, n_channels)
  # assert frame_shape == train_generator.im_shape
  stack_sizes = (n_channels, 48, 96, 192)
  R_stack_sizes = stack_sizes
  A_filt_sizes = (3, 3, 3)
  Ahat_filt_sizes = (3, 3, 3, 3)
  R_filt_sizes = (3, 3, 3, 3)
  layer_loss_weights = np.array([1., 0., 0., 0.])  # weighting for each layer in final loss; "L_0" model:  [1, 0, 0, 0], "L_all": [1, 0.1, 0.1, 0.1]
  layer_loss_weights = np.expand_dims(layer_loss_weights, 1)
  time_loss_weights = 1./ (nt - 1) * np.ones((nt,1))  # equally weight all timesteps except the first
  time_loss_weights[0] = 0

  predictor = prednet.prednet.PredNet(stack_sizes, R_stack_sizes,
                    A_filt_sizes, Ahat_filt_sizes, R_filt_sizes,
                    output_mode='error', return_sequences=True)
  
  inputs = keras.layers.Input(shape=(nt,) + frame_shape)
  errors = predictor(inputs)  # errors will be (batch_size, nt, nb_layers)
  errors_by_time = keras.layers.TimeDistributed(keras.layers.Dense(1, trainable=False),
                                                weights=[layer_loss_weights, np.zeros(1)],
                                                trainable=False,
                                                )(errors)  # calculate weighted error by layer
  errors_by_time = keras.layers.Flatten()(errors_by_time)  # will be (batch_size, nt)
  final_errors = keras.layers.Dense(1, weights=[time_loss_weights, np.zeros(1)], trainable=False)(errors_by_time)  # weight errors by time
  model = keras.models.Model(inputs=inputs, outputs=final_errors)
  model.compile(loss='mean_absolute_error', optimizer='adam')
  return model


def train_on_arrays_and_sources(train_file, train_sources, val_file, val_sources,
                                path_to_save_model_json='prednet_model.json', path_to_save_weights_hdf5='prednet_weights.hdf5',
                                model_path=None,
                                number_of_epochs=150, steps_per_epoch=125,
                                batch_size=4,
                                sequence_length=8,
                                max_validation_sequences=100,
                                ):
  """
  At most `max_validation_sequences` frame sequences will be used for validation.
  The validation data will be used at the end of each epoch.
  The loss function on validation data will be reported as val_loss.
  https://keras.io/models/sequential/#fit_generator
Epoch 3/8
16/16 [==============================] - 25s 2s/step - loss: 0.0064 - val_loss: 0.0064
Epoch 4/8
16/16 [==============================] - 27s 2s/step - loss: 0.0067 - val_loss: 0.0066

  `sequence_length` is the number of frames in each training sequence.
  """

  save_model = True  # if weights will be saved

  # Training parameters
  samples_per_epoch = steps_per_epoch * batch_size

  train_generator = prednet.data_utils.SequenceGenerator(train_file, train_sources,
                                                         sequence_length=sequence_length, batch_size=batch_size,
                                                         shuffle=True)
  val_generator = prednet.data_utils.SequenceGenerator(val_file, val_sources,
                                                       sequence_length=sequence_length, batch_size=batch_size,
                                                       max_num_sequences=max_validation_sequences)
  assert train_generator.im_shape == val_generator.im_shape

  if model_path and os.path.exists(model_path):
    model = keras.models.load_model(model_path, custom_objects = {'PredNet': prednet.prednet.PredNet})
  else:
    model = make_training_model(sequence_length, train_generator.im_shape)

  lr_schedule = lambda epoch: 0.001 if epoch < 75 else 0.0001    # start with lr of 0.001 and then drop to 0.0001 after 75 epochs
  callbacks = [keras.callbacks.LearningRateScheduler(lr_schedule)]
  if save_model:
      if os.path.dirname(path_to_save_weights_hdf5) != '' and not os.path.exists(os.path.dirname(path_to_save_weights_hdf5)):
        os.makedirs(os.path.dirname(path_to_save_weights_hdf5), exist_ok=True)
      # print('Setting keras.callbacks.ModelCheckpoint for', path_to_save_weights_hdf5)
      callbacks.append(keras.callbacks.ModelCheckpoint(filepath=path_to_save_weights_hdf5,
                                                       monitor='val_loss', save_best_only=True))
  else:
      raise NotImplementedError("It appears that evaluation requires the HDF5 file.")
  
  history = model.fit_generator(train_generator, steps_per_epoch, number_of_epochs,
                                callbacks=callbacks,
                  validation_data=val_generator,
                  # validation_steps=max_validation_sequences / batch_size,
                  # just run val_generator to exhaustion, it's already reduced to max_num_sequences
                  )

  if save_model:
      if model_path:
        if os.path.dirname(model_path) != '':
          os.makedirs(os.path.dirname(model_path), exist_ok=True)
        model.save(model_path)
        # When we upgrade to TensorFlow 2, this will need to specify HDF5 format: model.save(model_path, save_format='h5')
      if os.path.dirname(path_to_save_model_json) != '' and not os.path.exists(os.path.dirname(path_to_save_model_json)):
        os.makedirs(os.path.dirname(path_to_save_model_json), exist_ok=True)
      json_string = model.to_json()
      with open(path_to_save_model_json, "w") as f:
          f.write(json_string)
      model.save_weights(path_to_save_weights_hdf5)
      assert os.path.exists(path_to_save_weights_hdf5)
