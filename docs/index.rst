========
Contents
========

.. toctree::
   :maxdepth: 2

   readme
   installation
   usage
   reference/index
   contributing
   authors
   changelog
   example

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

