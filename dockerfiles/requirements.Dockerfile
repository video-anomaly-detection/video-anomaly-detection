ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=pythonpackagesonalpine
ARG DOCKER_BASE_IMAGE_NAME=basic-python-packages-pre-installed-on-alpine
ARG DOCKER_BASE_IMAGE_TAG=pip-alpine
FROM ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:${DOCKER_BASE_IMAGE_TAG}

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && apt-get install --assume-yes --no-install-recommends wget \
    && python -m pip install --no-cache-dir 'tensorflow-gpu>=1.13.1,<2.0' \
    && python -m pip install --no-cache-dir 'Keras>=2.2.4,<2.4.0' \
    && python -m pip install --no-cache-dir 'scipy>=1.2.0' \
    && python -m pip install --no-cache-dir requests bs4 jinja2 imageio imageio-ffmpeg pillow scikit-video ffmpeg-python matplotlib jupyterlab pytest \
    && . ./cleanup.sh
